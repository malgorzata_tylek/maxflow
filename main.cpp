#include <iostream>
#include <list>
#include <limits.h>
#include <stddef.h>
#include <cstdlib>
#include <sstream>
#include <string.h>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iostream>
#include <set>
#include <ctype.h>
#include <queue>

using namespace std;

struct edge {
    int from;
    int to;
    int weight;
};

struct edgeForPath {
    int from;
    int to;
};

const char COMMA = ',';

vector<int> splitByComma (string str, vector<int> array) {
    std::replace(str.begin(), str.end(), COMMA, ' ');

    array.clear();
    stringstream ss(str);
    int temp;
    while (ss >> temp) {
        array.push_back(temp);
    }

    return array;
}

vector<vector<int>> readMatrixFile (string in_file) {
    vector<edge> graph;
    ifstream in(in_file);
    string line;
    vector<int> splited;

    ifstream in2(in_file);

    int lineIterator = 0;
    int charIterator;
    std::vector<char> lineByChars;
    bool minus = false;

    int matrix_size = 4;
    vector< vector<int>> matrix;
    matrix.resize(matrix_size);
    for (int i = 0; i < matrix_size; i++) {
        matrix[i].resize(matrix_size);
    }

    while (getline(in2, line))
    {
        lineByChars.clear();
        std::copy(line.begin(), line.end(), std::back_inserter(lineByChars));

        charIterator = 0;
        for (int i = 0; i < lineByChars.size(); i++) {
            if (lineByChars.at(i) == COMMA) {
                charIterator = ++charIterator;
                matrix[lineIterator][charIterator] = NULL;
            }

            if (lineByChars.at(i) == '-') {
                minus = true;
            }

            if ((isdigit(lineByChars.at(i)))) {
                int numb = lineByChars.at(i) - '0';
                if (minus) {
                    matrix[lineIterator][charIterator] = -numb;
                    minus = false;
                } else {
                    matrix[lineIterator][charIterator] = numb;
                }
            }
        }

        lineIterator = ++lineIterator;
    }

    return matrix;
}


void printPath(vector<edgeForPath> paths) {
    cout << "Path: " << endl;
    cout << "size: " << paths.size() << endl;

    for (int i = 0; i < paths.size(); i++) {
        cout << " from: " << paths.at(i).from << " to: " << paths.at(i).to << endl;
    }
}

void printPath2(vector<edge> paths) {
    cout << "Path: " << endl;
    cout << "size: " << paths.size() << endl;

    for (int i = 0; i < paths.size(); i++) {
        cout << " from: " << paths.at(i).from << " to: " << paths.at(i).to << " weight: " << paths.at(i).weight << endl;
    }
}

vector<edge> readListFile (string in_file) {
    vector<edge> graph;
    ifstream in(in_file);
    string line;
    vector<int> splited;

    ifstream in2(in_file);

    while (getline(in2, line))
    {
        splited = splitByComma(line, splited);

        edge e;
        e.from = splited.at(0);
        e.to = splited.at(1);
        e.weight = splited.at(2);

        graph.push_back(e);
    }

    return graph;
}

bool areConnected(vector<edge> graph, int u, int v) {
    int e_count = 10;
    for (int i = 0; i < e_count; i++) {
        if (graph.at(i).from == u && graph.at(i).to == v && graph.at(i).weight > 0) {
            return true;
        }
    }
    return false;
}

bool bfsForMatrix(vector<vector<int>> matrix, int start_v, int end_v, vector<edgeForPath>& paths)
{
    int v_count = 4;
    // startowy wierzcholek odwiedzony
    vector<bool> visited {true, false, false, false};

    queue <int> queue_tmp;
    queue_tmp.push(start_v);

    // jest kolejny poziom galezi
    while (!queue_tmp.empty())
    {
        int u = queue_tmp.front();
        queue_tmp.pop();

        for (int v = 0; v < v_count; v++)
        {
            if (visited.at(v) == false && matrix[u][v] != NULL && matrix[u][v] >0)
            {
                queue_tmp.push(v);

                edgeForPath e;
                e.from = u;
                e.to = v;

                paths.push_back(e);

                visited.at(v) = true;
            }
        }
    }

    return (visited.at(end_v) == true);
}

bool bfsForGraph(vector<edge> graph, int start_v, int end_v, vector<edgeForPath>& paths)
{
    int v_count = 4;
    // startowy wierzcholek odwiedzony
    vector<bool> visited {true, false, false, false, false};

    queue <int> queue_tmp;
    queue_tmp.push(start_v);

    // jest kolejny poziom galezi
    while (!queue_tmp.empty())
    {
        int u = queue_tmp.front();
        queue_tmp.pop();

        for (int v = 0; v < v_count; v++)
        {
            if (visited.at(v) == false && areConnected(graph, u, v))
            {
                queue_tmp.push(v);

                edgeForPath e;
                e.from = u;
                e.to = v;

                paths.push_back(e);

                visited.at(v) = true;
            }
        }
    }

    return (visited.at(end_v) == true);
}

int getPrev(vector<edgeForPath> paths, int v) {
  //  printPath(paths);

    for (int i = 0; i < paths.size(); i++) {
        if (paths.at(i).to == v) {
            return paths.at(i).from;
        }
    }
    return -1;
}

int getPrev2(vector<edge> paths, int v) {
    for (int i = 0; i < paths.size(); i++) {
        if (paths.at(i).to == v) {
            return paths.at(i).from;
        }
    }
    return -1;
}

int getWeight(vector<edge> graph, int u, int v) {
    int e_count = 10;
    for (int i = 0; i < e_count; i++) {
        if (graph.at(i).from == u && graph.at(i).to == v) {
            return graph.at(i).weight;
        }
    }
    return 0;
}

vector<edge> updateWeight(vector<edge>& graph, int u, int v, int weight) {
    int e_count = 10;
    for (int i = 0; i < e_count; i++) {
        if (graph.at(i).from == u && graph.at(i).to == v) {
            graph.at(i).weight = weight;
        }
    }

    return graph;
}


int maxFlowForGraph(vector<edge> graph, int start_v, int end_v)
{
    int v_count = 4;
    int e_count = 10;
    int max_flow = 0;

    // graf rezydualny - aktualna przepustowosc -> w - f
    vector<edge> r_graph;
   // r_graph.resize(e_count);

    // na poczatku przeplyw poczatkowy sieci
    std::copy(graph.begin(), graph.end(), std::back_inserter(r_graph));

    vector<edgeForPath> paths;
  //  paths.resize(e_count);

    // dopoki sciezka w grafie w-f
    int u, v;
    while (bfsForGraph(r_graph, start_v, end_v, paths))
    {
        int e = INT_MAX;
        // znajdz minimalny przeplyw w sciezce => maksymalny przeplyw

        for (v = end_v; v != start_v; )
        {
            u = getPrev(paths, v);
            e = min(e, getWeight(r_graph, u, v));

            v = u;
        }

        for (v = end_v; v != start_v; )
        {
            u = getPrev(paths, v);
            int reversedFlow = getWeight(r_graph, v, u) - e;
            int updatedFlow = getWeight(r_graph, u, v) + e;

           // cout << "e " << e << endl;

            updateWeight(r_graph, u, v, reversedFlow);
            updateWeight(r_graph, v, u, updatedFlow);

            v = u;
        }
        max_flow = max_flow + e;
        paths.clear();

      //  cout << "max_flow " << max_flow << endl;
    }
    return max_flow;
}

int maxFlowForMatrix(vector<vector<int>> matrix, int start_v, int end_v)
{
    int v_count = 4;
    int max_flow = 0;

    vector<vector<int>> r_matrix;
    r_matrix.resize(4);

    for (int i = 0; i < v_count; i++) {
        r_matrix[i].resize(4);
    }

    // na poczatku przeplyw poczatkowy sieci
    for (int i = 0; i < v_count; i++) {
        for (int j = 0; j< v_count; j++) {
            r_matrix[i][j] = matrix[i][j];
        }
    }

    vector<edgeForPath> paths;

    int u, v;
    while (bfsForMatrix(r_matrix, start_v, end_v, paths))
    {
        int e = INT_MAX;
        // znajdz minimalny przeplyw w sciezce => maksymalny przeplyw

        for (v = end_v; v != start_v; )
        {
            u = getPrev(paths, v);
            e = min(e, r_matrix[u][v]);

            v = u;
        }

        for (v = end_v; v != start_v; )
        {
            u = getPrev(paths, v);
            int reversedFlow = r_matrix[v][u] + e;
            int updatedFlow = r_matrix[u][v] - e;

            r_matrix[v][u] = reversedFlow;
            r_matrix[u][v] = updatedFlow;

            v = u;
        }
        max_flow = max_flow + e;
        paths.clear();

    }
    return max_flow;
}


void test(vector<edge> graph) {
    bool con = areConnected(graph, 0,2);
    bool con2 = areConnected(graph, 1,2);
    bool con3 = areConnected(graph, 3,1);

   // cout << "con : " << con << " con2 : " << con2 << " con3 : " << con3 << endl;

    int a = getPrev2(graph, 0);
    int b = getPrev2(graph, 1);
    int c = getPrev2(graph, 2);
    int d = getPrev2(graph, 3);

  //  cout << "a : " << a << " b : " << b << " c : " << c << " d: " << d << endl;

    int e = getWeight(graph, 0, 1);
    int f = getWeight(graph, 1, 2);
    int g = getWeight(graph, 2, 1);
    int h = getWeight(graph, 3, 1);

  //  cout << "e : " << e << " f : " << f << " g : " << g << " h: " << h << endl;

    int i = getWeight(graph, 3, 1);
    vector<edge> graph2 = updateWeight(graph, 3, 1, 12);
    int j = getWeight(graph2, 3, 1);

    cout << "i : " << i << " j : " << j << endl;
}

int main()
{
    vector<edge> graph;
    graph = readListFile("listData.csv");

    int start_v = 0;
    int end_v = 2;
//
  //  test(graph);

    int maxFlow = maxFlowForGraph(graph, start_v, end_v);
    cout << "maxflow: " <<  maxFlow << endl;

    cout << "\n Matrix \n" << endl;
    vector<vector<int>> matrix;
    matrix = readMatrixFile("matrixData.csv");


    int maxFlowMatrix = maxFlowForMatrix(matrix, start_v, end_v);
    cout << "maxFlowMatrix: " <<  maxFlowMatrix << endl;

    return 0;
}
